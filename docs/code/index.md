# Code Overview

**Be aware:**
The code is being refactored and documented. Some codes may note work
properly. Thus, this page will be properly writen when the code refactor
finish. But by now the base modules are done (by now) and documented:

- [map.py](map.md): The bi-dimensional grid map implementation for Monte Carlo Localization.
- [simulator.py](simulator.md): The robot model simulator. It simulates the robot position, movements and sensor readings.
- [mcl.py](mcl.md): The Monte Carlo Localization algorithm implementation. Inherits the Simulator.
- [simulated-annealing.py](simulated-annealing.md): The implementation of the Simulated Annealing algorithm using the Boltzmann Distribution.