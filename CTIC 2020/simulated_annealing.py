from mcl import MCL
import numpy as np
from random import random


class Simu(MCL):

    def __init__(self, size, map_=50, passos=100, rand=True, mini=False, border=False):
        if rand:
            super().__init__(size, map_, border=border)
        else:
            map_ = np.zeros(size * size, 'uint32')
            map_.shape = size, size
            for i in range(0, size, 2):
                map_[i, :] = np.ones(size)
            super().__init__(size, map_=map_, border=border)
        self.__passos = range(passos)
        self.__best = self.mapa
        self.__now = self.mapa
        self.__best_value = self.analise()
        self.min = mini
        self.temp = 0.01
        self.draw_grid('0')

    def analise(self, cmd=-1):
        r = 2000  # 2000
        media_acertos = 0
        for i in range(100):  # 100
            self.atual = None
            acertos = 0
            for j in range(r):
                self.sense()
                self.move(cmd)
                if self.atual == self.estimated_position:
                    acertos += 1
            media_acertos += acertos / r
        return media_acertos / 100

    def new_estima(self, rodadas=1000):
        valor_old = self.best_value
        best_plot, rejeita_plot, analise_plot, aleatorio_plot = [self.__best_value], [0], [self.analise(-2)], [self.__best_value]
        rejeita = 0
        for i in range(1, rodadas):
            r = 0
            while True:
                array = np.nonzero(self.__now)
                sorte = np.random.choice(range(array[0].size))
                preto = (array[0][sorte], array[1][sorte])

                array = np.nonzero(1 - self.__now)
                sorte = np.random.choice(range(array[0].size))
                branco = (array[0][sorte], array[1][sorte])
                self.swap(branco, preto)
                valor = self.analise()
                if self.min != (self.__best_value < valor):
                    self.__best_value = valor
                    self.__best = self.mapa
                    valor_old = valor
                    self.__now = self.mapa
                    break
                elif self.min != (valor > valor_old or random() < np.exp((valor - valor_old) / self.temp)):
                    valor_old = valor
                    self.__now = self.mapa
                    break
                else:
                    self.swap(branco, preto)
                    r += 1
                    if not(r % 10):
                        print("Rejeitou 10 vezes seguidas")
                        print("Taxa de rejeicao: ", rejeita / i)
                        print("Rodada: ", i)
                    if r == 100:
                        analise_plot.append(self.analise(-2))
                        aleatorio_plot.append(valor)
                        return best_plot, rejeita_plot, analise_plot, aleatorio_plot
            self.draw_grid(str(i))
            rejeita_plot.append(r)
            best_plot.append(self.__best_value)
            analise_plot.append(self.analise(-2))
            aleatorio_plot.append(valor)
            rejeita += r
        return best_plot, rejeita_plot, analise_plot, aleatorio_plot

    @property
    def best_value(self):
        return self.__best_value

    @property
    def best(self):
        return self.__best.copy()

    @property
    def now(self):
        return self.__now.copy()
