"""
This main code do the analyses, the graphics, presented in my second published article:
 "Improving Monte Carlo Localization Performance Using Strategic Navigation Policies".

So, if you run the code after a day running you will see the graphics presented on paper.
"""

import matplotlib.pyplot as plt
import numpy as np

from copy import deepcopy
import os
from time import time

from gml import *
from mcl import MCL
from moving_matrix import MovingMatrix
from simulated_annealing import SimulatedAnnealing

"""Do Figure 3: Example of the tree generated to construct the optimal policy. The nodes have the image with the map 
where each state x with P x has a gray small square. The edges with s0 and s1 are a sensor reading white and black 
respectively. The edges with a0, a1, a2 and a3 are action to west, north, east and southrespectively. The red node 
denotes nodes where the robot has localized itself, although the distribution is different. 

This code generates a kind of directed acyclic graph (DAG) with cycle when the robot looses itself.

The DAG generated it is just an gml file, this approach was chosen to be more efficient.

The gml file is generated based on tree structure, exploring until the leaf with LIFO.
"""

iden = np.array([0, 1, 3, 11, 27, 91, 219, 731, 1755, 5851, 14043, 46811, 112347,
                 374491, 898779, 2995931, 7190235, 23967451, 57521883, 191739611], dtype=np.uint64)
def id(camada):
    global iden
    if camada >= 20:
        if camada % 2:
            return (id(camada - 1) - id(camada - 2)) * 4 + id(camada - 1)
        else:
            return (id(camada - 1) - id(camada - 2)) * 2 + id(camada - 1)
    else:
        return iden[camada]

os.chdir('dag/')
pasta = str(time())
os.mkdir(pasta)
os.chdir(pasta)

tam = 5
mcl = MCL(tam, tam**2 // 2)
im = mcl.get_image()

g = Graph()

mcl.draw_prob(im, "0")
g.nodes.append(Node(0, "First", 0, 0.0, 0.0, image="0"))

pilha = [mcl.prob_distribution]
k = 0
a = 0
s = 0

tot_c = int(np.ceil(np.log2(tam**2)) * 2 + 1)
total = id(tot_c) - 1

g.nodes.append(Node(total+1, "Sucesso", tam * 2 + 1, 0.0, 0.0))

camadas = np.zeros(tot_c, dtype=np.int64)
while camadas[k] != total:

    k += 1
    if k == (tot_c):
        pilha.pop()
        mcl.prob_distribution = pilha[-1]
        k -= 2
        continue

    if camadas[k] == 0:
        camadas[k] = id(k)
    else:
        camadas[k] += 1
    try:
        if (camadas[k] - id(k)) // (4 - 2 * (k % 2)) + id(k-1) != camadas[k-1]:
            if camadas[k] == id(k+1):
                break
            camadas[k] -= 1
            pilha.pop()
            mcl.prob_distribution = pilha[-1]
            k -= 2
            continue
    except RecursionError:
        print(k)
        print("Recursion Error")
        break

    go = camadas[k]

    if k % 2:
        s = (go - id(k)) % 2
        mcl.sense(s)
        ind = np.nonzero(mcl.prob_distribution)
        if ind[0].size == 1:
            mcl.prob_distribution = pilha[-1]
            g.edges.append(Edge(go, g.nodes[camadas[k - 1]], g.nodes[total + 1], "s%d" % s))
            for dest in range(k+1, tam * 2 + 1):
                camadas[dest] = (camadas[dest-1]-id(dest-1)+1)*(4 - 2 * (dest % 2))+id(dest)-1
            k -= 1
            continue
        elif ind[0].size == tam ** 2:
            mcl.prob_distribution = pilha[-1]
            g.edges.append(Edge(go, g.nodes[camadas[k - 1]], g.nodes[0], "s%d" % s))
            for dest in range(k+1, tam * 2 + 1):
                camadas[dest] = (camadas[dest-1]-id(dest-1)+1)*(4 - 2 * (dest % 2))+id(dest)-1
            k -= 1
            continue
        mcl.draw_prob(im, str(go))

        g.edges.append(Edge(go, g.nodes[camadas[k - 1]], Node(go, str(k), k, float(go * 100), float((k+go) * 100),
                                                              image=str(go)), "s%d" % s))
        pilha.append(mcl.prob_distribution)


    else:
        a = (go - id(k)) % 4
        mcl.move(a)
        mcl.draw_prob(im, str(go))
        g.edges.append(Edge(go, g.nodes[camadas[k - 1]], Node(go, str(k), k, float(go * 100), float((k+go) * 100),
                                                              image=str(go)), "a%d" % a))
        pilha.append(mcl.prob_distribution)

arq = open("dag.gml", 'w')
arq.write(str(g))
arq.close()
os.chdir('../..')


"""Do Figures 4 and 5: Performance in a scenario with noise in movement and sensor (left plot); performance in a 
scenario without noise (right plot). And Performance with noise just on movement (left plot) or just on sensor 
reading (right plot). 

Note that noise is not a parameter of MCL() constructor is a method defined at the beginning of the class definition. So
the code just makes one graphic, which one depends the noise parameters set on class definition.
"""

n = 10

TESTS = (1, 2)
TEST = len(TESTS)

r = 300
local = MCL(n, 1, border=False)
x_plot = np.fromiter(range(1, (n * n)//2 + 1, 2), dtype='int8')
# x_plot = np.fromiter(range(4, 12, 2), dtype='int8')

z_plot = np.zeros((TEST, x_plot.size))  # [[] for i in range(TEST)]

camadas = np.array([MovingMatrix(np.ones((n, n), dtype='int8')) for i in range(4 ** n)])


def gabarito(mcl, limite=n):
    mapa = mcl.map
    lePreto = MovingMatrix(mapa)
    leBranco = MovingMatrix(1 - mapa)
    camadas[0] = MovingMatrix(np.ones((n, n), dtype='int8'))
    camadas[0].zero = np.array(mcl.real_position)

    for k in range(1, limite + 1):
        ind = 0
        while ind < 4 ** limite:
            atual = camadas[ind]
            for mov in range(4):
                loop = deepcopy(atual)
                loop.move(mov)
                i = mov * 4 ** (limite - k)
                camadas[ind + i] = loop
                if mapa[tuple(camadas[ind + i].zero)]:
                    camadas[ind + i].matrix = camadas[ind + i] * lePreto
                else:
                    camadas[ind + i].matrix = camadas[ind + i] * leBranco
                if np.nonzero(camadas[ind + i].matrix)[0].size == 1:
                    return k

            ind += 4 ** (limite - k + 1)  # This fills the tree with space for the last layer nodes
    return -1


for p in x_plot:
    mediaPrimeiro = np.zeros(TEST)
    acertos = np.zeros(TEST)
    print(p)
    for i in range(200):  # 200
        local.map = p
        first = local.real_position
        gab = 1 #gabarito(local, n)
        if gab >= 0:
            #mediaPrimeiro[-1] += gab
            #acertos[-1] += 1
            for alg in range(TEST):
                local.map = local.map
                local.real_position = first
                local.reset_distribution()
                acertou = False
                for j in range(r):
                    local.move(-TESTS[alg])  # , draw_mov=i == 1)
                    local.sense()
                    if local.real_position == local.estimated_position:
                        mediaPrimeiro[alg] += j + 1
                        acertos[alg] += 1
                        acertou = True
                        # if i == 1:
                        # local.draw_grid('%d_alg%d' % (p, TESTS[alg]))
                        break
                if not (acertou): mediaPrimeiro[alg] += r
    print(acertos)
    for alg in range(TEST):
        # Como eu somo r se não achar, média deve ser do numero de vezes executada.
        z_plot[alg][p // 2] = (mediaPrimeiro[alg] / acertos[-1])

fig = plt.figure('', figsize=(9, 9))

plt.title('Tempo médio para o primeiro acerto ocorrer')
plt.xlabel('Quantidade de pontos pretos')
plt.ylabel('Tempo médio')
plt.plot(x_plot, z_plot[0], linewidth=2, label='Random')
plt.plot(x_plot, z_plot[1], linewidth=2, label='Our method')

# plt.plot(x_plot, z_plot[-1], linewidth=2, label='Alg-Gabarito')


plt.legend()
plt.show()
fig.savefig('newAnalisePrimeiroAcerto-200vezes-n=%d_paper.png' % n)


"""Do Figure 6: The graphic shows the experiment, the horizontal axis represents the number of asymmetry and vertical 
ones the first time to hit.

Note that this code is very similar to the code of Figure 4 e 5, the difference is the number of landmarks are fixed and
the number of asymmetries is what varies.
"""

n = 10
r = 300

TESTS = (1, 2)
TEST = len(TESTS)

grid = np.array([[1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
                 [0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
                 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
                 [0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
                 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
                 [0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
                 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
                 [0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
                 [1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
                 [0, 1, 0, 1, 0, 1, 0, 1, 0, 1]])

camadas = np.array([MovingMatrix(np.ones((n, n), dtype='int8')) for i in range(4 ** n)])


def gabarito(mcl, limite=n):
    mapa = mcl.map
    lePreto = MovingMatrix(mapa)
    leBranco = MovingMatrix(1 - mapa)
    camadas[0] = MovingMatrix(np.ones((n, n), dtype='int8'))
    camadas[0].zero = np.array(mcl.real_position)

    for k in range(1, limite + 1):
        ind = 0
        while ind < 4 ** limite:
            atual = camadas[ind]
            for mov in range(4):
                loop = deepcopy(atual)
                loop.move(mov)
                i = mov * 4 ** (limite - k)
                camadas[ind + i] = loop
                if mapa[tuple(camadas[ind + i].zero)]:
                    camadas[ind + i].matrix = camadas[ind + i] * lePreto
                else:
                    camadas[ind + i].matrix = camadas[ind + i] * leBranco
                if np.nonzero(camadas[ind + i].matrix)[0].size == 1:
                    return k

            ind += 4 ** (limite - k + 1)  # This fills the tree with space for the last layer nodes
    return -1


x_plot = np.array([1, 2, 4, 8, 16, 32])
z_plot = np.zeros((TEST + 1, x_plot.size))  # [[] for i in range(TEST)]
for k in range(50):
    trocas = 0
    mcl = MCL(n, map_=grid)
    for i in x_plot:
        mediaPrimeiro = np.zeros(TEST + 1)
        acertos = np.zeros(TEST + 1)
        while trocas != i:
            mapa = mcl.map
            array = np.nonzero(mapa)
            sorte = np.random.choice(range(array[0].size))
            preto = (array[0][sorte], array[1][sorte])

            array = np.nonzero(1 - mapa)
            sorte = np.random.choice(range(array[0].size))
            branco = (array[0][sorte], array[1][sorte])
            mcl.swap(branco, preto)
            trocas += 1
        mcl.draw_grid('%d trocas' % i)
        first = mcl.real_position
        gab = gabarito(mcl, n)
        if gab >= 0:
            mediaPrimeiro[-1] += gab
            acertos[-1] += 1
            for alg in range(TEST):
                mcl.real_position = first
                mcl.reset_distribution()
                acertou = False
                for j in range(r):
                    mcl.move(-TESTS[alg])  # , draw_mov=i == 1)
                    mcl.sense()
                    if mcl.real_position == mcl.estimated_position:
                        mediaPrimeiro[alg] += j + 1
                        acertos[alg] += 1
                        acertou = True
                        # if i == 1:
                        # local.draw_grid('%d_alg%d' % (p, TESTS[alg]))
                        break
                if not acertou: mediaPrimeiro[alg] += r
        for alg in range(TEST + 1):
            # Como eu somo r se não achar, média deve ser do numero de vezes executada.
            z_plot[alg][int(np.log2(i))] += mediaPrimeiro[alg]
z_plot = z_plot/30
fig = plt.figure('', figsize=(9, 9))

plt.title('Tempo médio para o primeiro acerto ocorrer')
plt.xlabel('Quantidade de trocas')
plt.ylabel('Tempo médio')
for alg in range(TEST):
    plt.plot(x_plot, z_plot[alg], linewidth=2, label='Alg-%d' % (TESTS[alg]))
plt.plot(x_plot, z_plot[-1], linewidth=2, label='Alg-Gabarito')

plt.legend()
plt.show()
fig.savefig('PrimeiroAcerto-200vezes-n=%d_trocas_paper5.png' % n)

"""Do Figure 7: The vertical axis is the hit rate and the horizontal ones is the number of landmarks.
"""

n = 10

y_plot = []
x_plot = []
z_plot = []
r = 2000
local = MCL(n, 1, border=False)
for p in range(1, n * n + 1, 2):
    mediaAcertos = 0
    mediaPrimeiro = 0
    for i in range(100):
        acertos = 0
        local.map = p
        for j in range(r):
            local.move()
            local.sense()
            if local.real_position == local.estimated_position:
                acertos += 1
                if acertos == 1:
                    mediaPrimeiro += j + 1
        mediaAcertos += acertos / r

    z_plot.append(mediaPrimeiro / 100)
    x_plot.append(p)
    y_plot.append(mediaAcertos / 100)

y_plot2 = []
x_plot2 = []
z_plot2 = []
local = MCL(n, 1, border=False)
for p in range(1, n * n + 1, 2):
    mediaAcertos = 0
    mediaPrimeiro = 0
    for i in range(100):
        acertos = 0
        local.map = p
        for j in range(r):
            local.move(cmd=-2)
            local.sense()
            if local.real_position == local.estimated_position:
                acertos += 1
                if acertos == 1:
                    mediaPrimeiro += j + 1
        mediaAcertos += acertos / r

    z_plot2.append(mediaPrimeiro / 100)
    x_plot2.append(p)
    y_plot2.append(mediaAcertos / 100)




fig = plt.figure('', figsize=(8, 8))
plt.title('Média de acertos ao longo de 2000 passos')
plt.xlabel('Quantidade de pontos pretos')
plt.ylabel('Média de acertos')
plt.plot(x_plot, y_plot, color='cornflowerblue', linewidth=2, label='Random')
plt.plot(x_plot2, y_plot2, linewidth=2, label='Our method')

plt.legend()
plt.show()
fig.savefig('AverageTime_Compare_r2000-100vezes-n=%d.png' %n)
